This page contains several **lightweight** SMTP agents (see [MailConcept](MailConcept))
which you can call from mutt via the "\$sendmail\" variable, if you
think that a full blown sendmail/postfix/qmail is overkill. Note, on
most systems an MTA is already installed to process system-mail, so you
might not need to mess around with yourself (maybe if you want to bypass
defaults)

Feel free to add your comments so that others can make a better decision
which one to
choose.

* nullclient ([postfix](http://www.postfix.org/STANDARD_CONFIGURATION_README.html#null_client), [sendmail](http://www.sendmail.com/sm/open_source/docs/m4/features.html))  
 * sendmail can be configured to simply pass on all mail and do nothing else fancy, see **nullclient** feature in cf/README or postfix-doc.  
 * This *might* save you an installation of extra software, since sendmail or postfix is installed by default on many systems.  
* ssmtp (https://wiki.debian.org/sSMTP)  
 * Manual is in the source tarball. Supports TLS, IPv6 and MD5-auth. Optionally rewrites From: header.  
 * SMTP Password has to be saved in a configuration file. No comandline authentification possible.  
* nullmailer <http://untroubled.org/nullmailer/>  
 * Supports SMTP AUTH PLAIN as of 1.02. No SSL.
* nbsmtp (http://nbsmtp.ferdyx.org/)  
 * Supports TLS, SYSLOG, SASL, IPv6, STARTTLS, no need to configure it even though it supports a config file.
* msmtp (http://msmtp.sourceforge.net/)  
 * Supports TLS, IPv6, DSN, various authentication methods. One-line-configuration in .muttrc; feature that autochooses the server according to $from.  
 * IMPORTANT! will refuse to send your mail if you've tls in your ~/.msmtprc and SMTP server has TLS certificate that you don't trust.  
  * To get around this, either trust the certificate, or you can add tls_nocertcheck to your ~/.msmtprc.
* masqmail (http://freecode.com/projects/masqmail)  
 * Not really lightweight, a full MTA, depends on Glib (no, not Gtk)  
* smtppush(.py) (http://jclement.ca/software/smtppush.py/)  
 * A cross-platform clone of Michael Elkins' SMTPPUSH program.  
* sendmail(.py) (http://www.ynform.org/w/Pub/SendmailPy)  
 * A python script, compatible with mutt, which uses the most secure channel it can find  
* Putmail (http://putmail.sourceforge.net/)  
 * Simple configuration, Multiplatform. Written in Python. Optional specific settings depending on the "From:" header.  
 * Supports SMTP authentication, TLS and IPv6 and has an optional, simple queuing system.  
* DMA (!DragonFly Mail Agent, <http://packages.debian.org/de/sid/dma>)  
 * TLS/SSL and SMTP authentication, /etc/dma/{dma.conf,auth.conf,virtusertable}  
 * set sendmail="/usr/sbin/dma -Ac -i"  
* Exim (http://www.exim.org/)  
 * Exim is a full-blown MTA, but has a configuration option (mua_wrapper) to behave as a simple non-queuing SMTP client.  
 * <http://www.exim.org/exim-html-current/doc/html/spec_html/ch48.html>

* ssh: if you don't have local agent but want to use on remote site.

 
  `'set sendmail="ssh remotehost sendmail ..."'`


 * authentification (optional only once) possible with ssh keys; won't work with all systems, you can't reach all smtp servers with ssh.
