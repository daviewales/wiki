### Why are there strange files (\*mutt\*~ or \*mutt\*.bak) when mutt finishes?

Either mutt crashed (rare), or your editor produces backup files while
you edit. In Emacs, you can inhibit the creation of these files with
something like the following elisp in your .emacs file.

    (defun mutt-mail-mode-hook ()
      (flush-lines "^\\(> \n\\)*> -- \n\\(\n?> .*\\)*") ; kill quoted sigs
      (not-modified)
      (mail-text)
      (setq make-backup-files nil))
    (or (assoc "mutt-" auto-mode-alist)
        (setq auto-mode-alist (cons '("/tmp/mutt.*" . mail-mode) auto-mode-alist)))
    (add-hook 'mail-mode-hook 'mutt-mail-mode-hook)

### How do I configure Mutt to use mail-mode in Emacs?

You actually have to configure both Mutt and Emacs as shown here:

    From: Nick Willson <nick[]nickwillson.com>
    Date: Tue Sep 09 2003 - 15:05:17 EDT
    
    On 2003-09-09 13:23, Alex Malinovich wrote:
    
    > 2) Is there any way to have mutt pass options to emacs when invoking
    > my EDITOR? I'd like to automatically be in mail-mode with auto-fill
    > enabled whenever emacs is called from mutt. (if anyone has suggestions
    > for better modes to use, please do tell me) Is there a way of having
    > mutt tell this to emacs?
    
    This works
    
    In .muttrc:
    
       set editor="emacs -nw --no-init-file --load ~nick/.emacs-mail +7:0"
    
    In .emacs-mail:
    
       ( setq
         auto-mode-alist
         ( cons '("/tmp/mutt.*$" . post-mode) auto-mode-alist )
       )
       (add-hook 'mail-mode-hook 'turn-on-auto-fill)

Source: <http://marc.info/?l=debian-user&r=34&b=200309&w=2> - 20.6.2004

You can also use something like the elisp in the last question.

It is probably a better idea to use emacsclient or gnuclient to connect
to your running emacs, than to run **emacs -nw** for every message.

### How to trim quoted replies (like stripping signatures)?

Mutt can only add **$attribution, $indent\_string, $signature** to
replies, it can not reduce quoted text. But you can use your editor to
do it for you (vim can do that), or you write a small script that
digests replies before they are passed to the
editor:

`muttrc: set editor=trim-script`  
`trim-script: does something with $1, calls or "exec"s editor with the result, which has to be stored again in file passed as $1`

Emacs has a robust facility for manipulating quoted replies called
supercite. (C-h i / supercite RET).
