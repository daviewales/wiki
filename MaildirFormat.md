If you want the technical details of the maildir format, see the
following resources.

* <http://cr.yp.to/proto/maildir.html>  
* <http://www.qmail.org/man/man5/maildir.html>

Basically, maildir format is a set of directories with the layout

``` 
 Maildir/
         cur/
         new/
         tmp/
```

Each message is stored in a separate file in either the *cur* or *new*
directories, depending on whether or not it has been seen by the mail
client. Maildir folders do not need file locking (no ':0:' in
.procmailrc). The *tmp* dir is used internally when writing new messages
to the folder.

You do not have to configure anything to get Mutt to read maildir format
mailboxes. It can determine the mailbox type automatically by inspecting
the directory structure.

To create a maildir format mailbox, either:

* **set
mbox_type="maildir"** and create a new folder in Mutt  
* use procmail and append '/' to the folder name  
* `mkdir -p testbox/{cur,new,tmp}&& chmod -R 0700 testbox`   
* use the [maildirmake](http://www.courier-mta.org/maildirmake.html) program as included in maildrop and Courier or the simple script included in dovecot

[MaildirFormat](MaildirFormat) is one of a few which Mutt supports, see [FolderFormat](FolderFormat). See
also [Extended[MaildirFormat](MaildirFormat)](Extended[MaildirFormat](MaildirFormat)) (maildir with subfolders). See
[OverloadedNames](OverloadedNames) for other uses of the term
"maildir".

### MDAs that support delivery to Maildirs

* [procmail](http://www.procmail.org/)  
 * well known/widespread, built-in support in many MTAs (sendmail etc.)  
 * ([un]limited) support of [Extended[MaildirFormat](MaildirFormat)](Extended[MaildirFormat](MaildirFormat)) by manually encoding the correct directory (ie. $MAILDIR/.Inbox.Work.Emails\ From\ Boss.Important/)  
* [maildrop](http://www.courier-mta.org/maildrop/)  
 * easier to read (and understand) filter language  
 * checks syntax of the filter file before attempting delivery, deferring delivery on error - lower chance to loose mail  
 * lower resource usage when handling large messages  
 * supports [Extended[MaildirFormat](MaildirFormat)](Extended[MaildirFormat](MaildirFormat)) and includes [maildirmake](http://www.courier-mta.org/maildirmake.html).
