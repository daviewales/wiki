## The proposition: systematic variables naming scheme

Some mutt config vars need more intuitive names. Some vars are already
going to be changed. While changing a set already, which will happen
anyway, you might as well go through all of them and do it right.

The idea is to prefix all variables with category names, so that --
after sorting them alphabetically -- the manual provides faster access
to related variables when users have to examine several causes/
solutions for problems.

Also, insert "use\_" for all optional features controlling variable
names (example: use\_from, use\_my\_hdr), and "\_cmd" for external
commands.

## The drawback: onetime config file upgrade, temporary confusion by old docs

This project would change most of currently present variables' names.
Once the transition time has passed (a \_long\_ time!), meaning old
variable names would not be valid anymore, **every user** would be
required **once** to adapt their muttrc config files to the new names.

The official documentation will change immediately to use the new names.
Old uses and references from examples and guides on the net will cause
confusion until they are fixed.

## The benefit: easier to find solutions

... comes through both the proximity in the manual and TOC as well as
range of related names by the category prefix (and naturally from easier
to recognize intuitive var
names):

* for newbies (and forgetful oldies) to find them on their own.  
* for helpers to point newbies into the right direction.  
* get new ideas to improve personal setup by finding related causes and solutions for problems.  
* better guess/ understanding of examples without having to check manual.

## Technical solutions to "ease the pain"

Every change should be made as painless as possible. The better the
support, the better its acceptance and success. To make a smooth
integration in the release process stuff must be prepared **ahead of
time**. Review and improve the 2 lists to make it happen pleasantly.
Please **carefully read and edit(!)** these related
pages:

\[[VarNames/List](VarNames/List)\]::

 var-names suggestions: updates for most recent versions are especially welcome!

\[[VarNames/RoadMap](VarNames/RoadMap)\]::

 action plan of what should be done before the change can happen.

## Discussion

Decide *just for yourself, you personally, individually* whether you can
take this one-time pain! Please **carefully read and edit(!)** these
related pages:

\[[VarNames/Discussion](VarNames/Discussion)\]::

 of **what this project (and vote) is
about** and what it is NOT!  
 A collection of pro's and con's and negations/ invalidations to help you make a **well
reasoned choice**.

\[[VarNames/Vote](VarNames/Vote)\]::

 about **preference to
support** NEWBIES or OLDIES.  
 Can you get over changing your config **once** in favour of newbies?

For all those pages applies: edit/ add as you see fit.

-----

Note: there is no *code-change* involved with this, it just changes the
user-interface of the muttrc and the manual order (content and TOC
controlled by init.h). Historians can have a look at the \#1263 for the
origins.
